﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Class_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smocks;
using Smocks.Matching;
using System.Diagnostics;

namespace Class_Library.Tests
{
  [TestClass()]
  public class Class1Tests
  {
    [TestMethod()]
    public void MyFunctionTest_oddhour()
    {
      Smock.Run(context =>
      {
        context.Setup(() => It.IsAny<Class1>().MyAnotherFunction()).Returns("My Mocked String");
        context.Setup(() => It.IsAny<Class1>().MyAdditionFunction(It.IsAny<int>(), It.IsAny<int>())).Returns(1);        
        new Class1().MyFunction(3);
        context.Setup(() => It.IsAny<Class1>().MyAnotherFunction()).Returns("My Mocked String");
        context.Setup(() => It.IsAny<Class1>().MyAdditionFunction(It.IsAny<int>(), It.IsAny<int>())).Returns(2);
        new Class1().MyFunction(4);
        context.Setup(() => It.IsAny<Class1>().MyAnotherFunction()).Returns("My Mocked String");
        context.Setup(() => It.IsAny<Class1>().MyAdditionFunction(It.IsAny<int>(), It.IsAny<int>())).Returns(1);
        context.Setup(() => It.IsAny<Class1>().MyAnotherFunction()).Throws<Exception>(new Exception());
        bool isExceptionRaised = false;
        try
        {
          new Class1().MyFunction(3);
        }
        catch (Exception)
        {
          isExceptionRaised = true;
        }
        Assert.IsTrue(isExceptionRaised);
      });
    }
    

    [TestMethod()]
    public void MyAnotherFunctionTest()
    {
      Smock.Run(context =>
      {        
        string returnstring = new Class1().MyAnotherFunction();
        Assert.AreEqual(returnstring, "Actual String that I want to Return");
      });
    }

    [TestMethod()]
    public void MyAdditionFunctionTest()
    {
      Smock.Run(context =>
      {       
        int returnstring = new Class1().MyAdditionFunction(1,2);
        Assert.AreEqual(returnstring, 3);
      });
    }
  }
}