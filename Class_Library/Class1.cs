﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_Library
{
  public class Class1
  {
    public Class1()
    {
      //do nothing
    }
    public void MyFunction(int input)
    {
      string a = "hello world";
      Console.WriteLine(a);
      Console.WriteLine(MyAnotherFunction());
      if (input % 2 == 0)
      {
        Console.WriteLine(MyAdditionFunction(1, 2));
      }
      else
      {
        Console.WriteLine(MyAdditionFunction(3, 4));
      }
    }

    public string MyAnotherFunction()
    {
      string x = "Actual String that I want to Return";
      return x;
    }

    public int MyAdditionFunction(int a, int b)
    {
      return a + b;
    }
  }
}
